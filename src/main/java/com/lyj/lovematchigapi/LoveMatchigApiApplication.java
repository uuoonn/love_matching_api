package com.lyj.lovematchigapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LoveMatchigApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(LoveMatchigApiApplication.class, args);
    }

}

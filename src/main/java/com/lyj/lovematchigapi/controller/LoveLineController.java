package com.lyj.lovematchigapi.controller;

import com.lyj.lovematchigapi.entity.Member;
import com.lyj.lovematchigapi.model.loveLine.LoveLineCreateRequest;
import com.lyj.lovematchigapi.model.loveLine.LoveLineItem;
import com.lyj.lovematchigapi.model.loveLine.LoveLinePhoneNumberChangeRequest;
import com.lyj.lovematchigapi.model.loveLine.LoveLineResponse;
import com.lyj.lovematchigapi.service.LoveLineService;
import com.lyj.lovematchigapi.service.MemberService;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/love-line")
    public class LoveLineController {
        private final MemberService memberService;
        private final LoveLineService loveLineService;

        @PostMapping("/new/member-id/{memberId}")
        public String setLoveLine(@PathVariable long memberId, @RequestBody LoveLineCreateRequest request){
            Member member = memberService.getData(memberId);
            loveLineService.setLoveLine(member,request);

            return "전송완료";
        }


        @GetMapping("/all")
        public List<LoveLineItem> getLoveLines(){
            return loveLineService.getLoveLines();
        }

        @GetMapping("/detail/{id}")
        public LoveLineResponse getLoveLine(@PathVariable long id){
            return loveLineService.getLoveLine(id);
        }


        @PutMapping("/change-number-id/{loveLineId}")
        public String putPhoneNumber(@PathVariable long loveLineId, @RequestBody LoveLinePhoneNumberChangeRequest request){
            loveLineService.putPhoneNumber(loveLineId,request);

            return "번호변경완료";
        }


}

package com.lyj.lovematchigapi.controller;

import com.lyj.lovematchigapi.model.Member.MemberChangeRequest;
import com.lyj.lovematchigapi.model.Member.MemberCreateRequest;
import com.lyj.lovematchigapi.model.Member.MemberItem;
import com.lyj.lovematchigapi.model.Member.MemberResponse;
import com.lyj.lovematchigapi.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.time.format.SignStyle;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberService memberService;

    @PostMapping("/join")
    public String setMember(@RequestBody MemberCreateRequest request){
        memberService.setMember(request);

        return "정보입력완료";
    }

    @GetMapping("/all")
    public List<MemberItem> getMembers(){
       return memberService.getMembers();


    }

    @GetMapping("/detail/{id}")
    public MemberResponse getMember(@PathVariable long id){
        return memberService.getMember(id);
    }

    @PutMapping("/edit-my-phone/{id}")
    public String putMyPhoneNumber(@PathVariable long id, @RequestBody MemberChangeRequest request){
        memberService.putMyPhoneNumber(id,request);

        return "번호수정완료";
    }

    @DeleteMapping("/del/{id}")
    public String delMember(@PathVariable long id){
        memberService.delMember(id);

        return "정보 삭제 완료";
    }


}

package com.lyj.lovematchigapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter

public class LoveLine {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId" , nullable = false)
    private Member member;
    //privte~~ 위의 값만 쓰면 타입부분에 빨간 줄 뜸
    //ManyToOne : 다대일/fetch->어떤 타입이랑 다닐건지~
    //joincolumn 컬럼인데 join된 컬럼이다.
    // 컬럼 이름이 memberId인 것.(Jpa의 영역)

    @Column(nullable = false, length = 13)
    private String lovePhoneNumber;
}

package com.lyj.lovematchigapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor

public enum Gender {
    MAN("남"),
    WOMAN("여");

    private String name;
}

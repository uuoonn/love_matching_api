package com.lyj.lovematchigapi.model.Member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class MemberChangeRequest {
    private String phoneNumber;
}

package com.lyj.lovematchigapi.model.Member;

import com.lyj.lovematchigapi.enums.Gender;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class MemberCreateRequest {
    private String name;
    private String phoneNumber;
    private Gender genderName;
}

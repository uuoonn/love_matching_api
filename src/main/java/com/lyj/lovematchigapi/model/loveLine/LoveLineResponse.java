package com.lyj.lovematchigapi.model.loveLine;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class LoveLineResponse {
    private Long memberId;
    private String memberName;
    private String memberPhoneNumber;
    private String lovePhoneNumber;

}

package com.lyj.lovematchigapi.repository;

import com.lyj.lovematchigapi.entity.LoveLine;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LoveLineRepository extends JpaRepository<LoveLine,Long> {
}

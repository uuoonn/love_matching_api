package com.lyj.lovematchigapi.repository;

import com.lyj.lovematchigapi.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository<Member,Long> {
}

package com.lyj.lovematchigapi.service;

import com.lyj.lovematchigapi.entity.LoveLine;
import com.lyj.lovematchigapi.entity.Member;
import com.lyj.lovematchigapi.model.loveLine.LoveLineCreateRequest;
import com.lyj.lovematchigapi.model.loveLine.LoveLineItem;
import com.lyj.lovematchigapi.model.loveLine.LoveLinePhoneNumberChangeRequest;
import com.lyj.lovematchigapi.model.loveLine.LoveLineResponse;
import com.lyj.lovematchigapi.repository.LoveLineRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class LoveLineService {
    private final LoveLineRepository loveLineRepository;

    public void setLoveLine(Member member, LoveLineCreateRequest request){
        LoveLine addData = new LoveLine();
        addData.setMember(member);
        addData.setLovePhoneNumber(request.getLovePhoneNumber());

        loveLineRepository.save(addData);
    }

    public List<LoveLineItem> getLoveLines(){
        List<LoveLine> originList = loveLineRepository.findAll();

        List<LoveLineItem> result = new LinkedList<>();

        for(LoveLine loveLine:originList) {
            LoveLineItem addItem = new LoveLineItem();
            addItem.setId(loveLine.getId());
            addItem.setMemberId(loveLine.getMember().getId());
            addItem.setMemberName(loveLine.getMember().getName());
            addItem.setMemberPhoneNumber(loveLine.getMember().getPhoneNumber());
            addItem.setLovePhoneNumber(loveLine.getLovePhoneNumber());

            result.add(addItem);
        }

        return result;

    }

    public LoveLineResponse getLoveLine(long id){
        LoveLine loveLine = loveLineRepository.findById(id).orElseThrow();

        LoveLineResponse response = new LoveLineResponse();

        response.setMemberId(loveLine.getMember().getId());
        response.setMemberName(loveLine.getMember().getName());
        response.setMemberPhoneNumber(loveLine.getMember().getPhoneNumber());
        response.setLovePhoneNumber(loveLine.getLovePhoneNumber());

        return response;
    }

    public void putPhoneNumber(long id, LoveLinePhoneNumberChangeRequest request){
        LoveLine originData = loveLineRepository.findById(id).orElseThrow();
        originData.setLovePhoneNumber(request.getLovePhoneNumber());

        loveLineRepository.save(originData);
    }
}

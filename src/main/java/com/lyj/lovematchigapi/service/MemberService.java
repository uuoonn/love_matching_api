package com.lyj.lovematchigapi.service;

import com.lyj.lovematchigapi.entity.Member;
import com.lyj.lovematchigapi.model.Member.MemberChangeRequest;
import com.lyj.lovematchigapi.model.Member.MemberCreateRequest;
import com.lyj.lovematchigapi.model.Member.MemberItem;
import com.lyj.lovematchigapi.model.Member.MemberResponse;
import com.lyj.lovematchigapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.DeleteMapping;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor

public class MemberService {
    private final MemberRepository memberRepository;


    public Member getData(long id){
        return memberRepository.findById(id).orElseThrow();
    }

    public void setMember(MemberCreateRequest request) {
        Member addData = new Member();
        addData.setName(request.getName());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setGenderName(request.getGenderName());

        memberRepository.save(addData);
    }

    public List<MemberItem> getMembers(){
        List<Member> originList = memberRepository.findAll();
            List<MemberItem> result = new LinkedList<>();
            for(Member member : originList){
            MemberItem addItem = new MemberItem();
            addItem.setId(member.getId());
            addItem.setName(member.getName());
            addItem.setPhoneNumber(member.getPhoneNumber());
            addItem.setGenderName(member.getGenderName().getName());

            result.add(addItem);

            }
        return result;
    }

    public MemberResponse getMember(long id){
        Member originData = memberRepository.findById(id).orElseThrow();
        MemberResponse response = new MemberResponse();
        response.setId(originData.getId());
        response.setName(originData.getName());
        response.setPhoneNumber(originData.getPhoneNumber());
        response.setGenderName(originData.getGenderName().getName());

        return response;
    }

    public void putMyPhoneNumber(Long id, MemberChangeRequest request){
        Member originData = memberRepository.findById(id).orElseThrow();
        originData.setPhoneNumber(request.getPhoneNumber());

        memberRepository.save(originData);
    }

    public void delMember(long id){
        memberRepository.deleteById(id);
    }

}
